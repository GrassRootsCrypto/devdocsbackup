---
description: This covers BNB and Cosmos connections
---

# BFT Chains

## Assets <a href="#assets" id="assets"></a>

Assets on BFT chains are generally native tokens (not contract-based), and can pay their fee in their native tokens. Thus there is no router required.

#### Re-orgs <a href="#re-orgs" id="re-orgs"></a>

BFT chains do not re-org so no re-org logic is required.

#### Conf-counting <a href="#conf-counting" id="conf-counting"></a>

BFT chains do not require any confirmations.

#### Gas <a href="#gas" id="gas"></a>

BFT chains generally have static fees, so fee rates don't need to be updated regularly.
